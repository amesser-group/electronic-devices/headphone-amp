Class A Headphone amplifier
===========================

This project was inspired by a discussion on 
mikrocontroller.net. It is a Class A Headphone amplifier
build around a Logic-Level Mosfet Output stage.

Status
------

Spice simulated, schematic and pcb designed, assembled
and confirmed working :-)

License
-------

CC BY-SA for all files in /hardware subfolder

Copyright
---------

(c) 2019 Andreas Messer <andi@bastelmap.de>

