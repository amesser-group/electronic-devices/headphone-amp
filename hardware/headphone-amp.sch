EESchema Schematic File Version 4
LIBS:headphone-amp-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Class A Headphone Amplifier"
Date "2019-12-30"
Rev "1"
Comp "(c) 2019 Andreas Messer"
Comment1 "HPA-1"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L headphone-amp-rescue:Q_NMOS_GDS Q4
U 1 1 5DDD4BA7
P 2450 6850
F 0 "Q4" H 2650 6950 50  0000 L CNN
F 1 "IRLR024N" H 2650 6850 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2" H 2650 6950 50  0001 C CNN
F 3 "" H 2450 6850 50  0001 C CNN
	1    2450 6850
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R13
U 1 1 5DDD4C75
P 2550 6400
F 0 "R13" V 2630 6400 50  0000 C CNN
F 1 "27" V 2550 6400 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 2480 6400 50  0001 C CNN
F 3 "" H 2550 6400 50  0001 C CNN
	1    2550 6400
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R10
U 1 1 5DDD4D0E
P 2350 6400
F 0 "R10" V 2430 6400 50  0000 C CNN
F 1 "27" V 2350 6400 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 2280 6400 50  0001 C CNN
F 3 "" H 2350 6400 50  0001 C CNN
	1    2350 6400
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R14
U 1 1 5DDD4D44
P 2550 7300
F 0 "R14" V 2630 7300 50  0000 C CNN
F 1 "27" V 2550 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 2480 7300 50  0001 C CNN
F 3 "" H 2550 7300 50  0001 C CNN
	1    2550 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R11
U 1 1 5DDD4D92
P 2350 7300
F 0 "R11" V 2430 7300 50  0000 C CNN
F 1 "27" V 2350 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 2280 7300 50  0001 C CNN
F 3 "" H 2350 7300 50  0001 C CNN
	1    2350 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R8
U 1 1 5DDD4E4C
P 2000 7100
F 0 "R8" V 1900 7100 50  0000 C CNN
F 1 "10" V 2000 7100 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1930 7100 50  0001 C CNN
F 3 "" H 2000 7100 50  0001 C CNN
	1    2000 7100
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R6
U 1 1 5DDD4F08
P 1800 7300
F 0 "R6" V 1880 7300 50  0000 C CNN
F 1 "2.2k" V 1800 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1730 7300 50  0001 C CNN
F 3 "" H 1800 7300 50  0001 C CNN
	1    1800 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R7
U 1 1 5DDD4FDF
P 2000 6600
F 0 "R7" V 2080 6600 50  0000 C CNN
F 1 "2.2k" V 2000 6600 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1930 6600 50  0001 C CNN
F 3 "" H 2000 6600 50  0001 C CNN
	1    2000 6600
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R3
U 1 1 5DDD5219
P 1450 7300
F 0 "R3" V 1530 7300 50  0000 C CNN
F 1 "220k" V 1450 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1380 7300 50  0001 C CNN
F 3 "" H 1450 7300 50  0001 C CNN
	1    1450 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R1
U 1 1 5DDD5269
P 1250 6850
F 0 "R1" V 1330 6850 50  0000 C CNN
F 1 "150k" V 1250 6850 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1180 6850 50  0001 C CNN
F 3 "" H 1250 6850 50  0001 C CNN
	1    1250 6850
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:R R4
U 1 1 5DDD52EA
P 1600 6600
F 0 "R4" V 1680 6600 50  0000 C CNN
F 1 "1k" V 1600 6600 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1530 6600 50  0001 C CNN
F 3 "" H 1600 6600 50  0001 C CNN
	1    1600 6600
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:CP C2
U 1 1 5DDD54C8
P 1600 6350
F 0 "C2" V 1750 6200 50  0000 L CNN
F 1 "47µ" V 1650 6150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 1638 6200 50  0001 C CNN
F 3 "" H 1600 6350 50  0001 C CNN
	1    1600 6350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DDD5714
P 2550 7500
F 0 "#PWR01" H 2550 7250 50  0001 C CNN
F 1 "GND" H 2550 7350 50  0001 C CNN
F 2 "" H 2550 7500 50  0001 C CNN
F 3 "" H 2550 7500 50  0001 C CNN
	1    2550 7500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DDD57A0
P 2350 7500
F 0 "#PWR02" H 2350 7250 50  0001 C CNN
F 1 "GND" H 2350 7350 50  0001 C CNN
F 2 "" H 2350 7500 50  0001 C CNN
F 3 "" H 2350 7500 50  0001 C CNN
	1    2350 7500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5DDD585D
P 1800 7500
F 0 "#PWR03" H 1800 7250 50  0001 C CNN
F 1 "GND" H 1800 7350 50  0001 C CNN
F 2 "" H 1800 7500 50  0001 C CNN
F 3 "" H 1800 7500 50  0001 C CNN
	1    1800 7500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5DDD5892
P 1450 7500
F 0 "#PWR04" H 1450 7250 50  0001 C CNN
F 1 "GND" H 1450 7350 50  0001 C CNN
F 2 "" H 1450 7500 50  0001 C CNN
F 3 "" H 1450 7500 50  0001 C CNN
	1    1450 7500
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:C C1
U 1 1 5DDD660A
P 1250 7050
F 0 "C1" V 1300 6900 50  0000 L CNN
F 1 "4.7µ" V 1400 6900 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L26.5mm_W7.0mm_P22.50mm_MKS4" H 1288 6900 50  0001 C CNN
F 3 "" H 1250 7050 50  0001 C CNN
	1    1250 7050
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R2
U 1 1 5DDEB759
P 1400 6150
F 0 "R2" V 1300 6150 50  0000 C CNN
F 1 "10k" V 1400 6150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1330 6150 50  0001 C CNN
F 3 "" H 1400 6150 50  0001 C CNN
	1    1400 6150
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R5
U 1 1 5DDEB9F9
P 1750 5950
F 0 "R5" V 1650 5950 50  0000 C CNN
F 1 "2.2k" V 1750 5950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1680 5950 50  0001 C CNN
F 3 "" H 1750 5950 50  0001 C CNN
	1    1750 5950
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:CP C4
U 1 1 5DDEC235
P 2000 6150
F 0 "C4" H 2025 6250 50  0000 L CNN
F 1 "47µ" H 2025 6050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 2038 6000 50  0001 C CNN
F 3 "" H 2000 6150 50  0001 C CNN
	1    2000 6150
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:D_Zener D5
U 1 1 5DDEC524
P 2850 5150
F 0 "D5" H 3050 5200 50  0000 C CNN
F 1 "8.2V" H 3000 5100 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-41_SOD81_P10.16mm_Horizontal" H 2850 5150 50  0001 C CNN
F 3 "" H 2850 5150 50  0001 C CNN
	1    2850 5150
	0    -1   1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R15
U 1 1 5DDECA17
P 2850 4250
F 0 "R15" V 2930 4250 50  0000 C CNN
F 1 "150" V 2850 4250 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2780 4250 50  0001 C CNN
F 3 "" H 2850 4250 50  0001 C CNN
	1    2850 4250
	-1   0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R12
U 1 1 5DDED377
P 2500 5150
F 0 "R12" V 2400 5150 50  0000 C CNN
F 1 "10k" V 2500 5150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2430 5150 50  0001 C CNN
F 3 "" H 2500 5150 50  0001 C CNN
	1    2500 5150
	-1   0    0    -1  
$EndComp
Text Label 1400 5950 0    60   ~ 0
VREF
Wire Wire Line
	2350 6550 2350 6600
Wire Wire Line
	2150 6600 2350 6600
Wire Wire Line
	2550 6550 2550 6600
Connection ~ 2550 6600
Wire Wire Line
	2550 7050 2550 7100
Wire Wire Line
	2350 7100 2550 7100
Wire Wire Line
	2350 7100 2350 7150
Connection ~ 2550 7100
Wire Wire Line
	1850 7100 1800 7100
Wire Wire Line
	1800 7050 1800 7100
Connection ~ 1800 7100
Connection ~ 2350 6600
Wire Wire Line
	2250 6850 2200 6850
Wire Wire Line
	2200 6850 2200 7100
Wire Wire Line
	2200 7100 2150 7100
Wire Wire Line
	1750 6600 1800 6600
Wire Wire Line
	1800 6600 1800 6650
Wire Wire Line
	1400 6850 1450 6850
Wire Wire Line
	1450 6850 1450 7050
Connection ~ 1450 6850
Connection ~ 1800 6600
Wire Wire Line
	1050 6550 1050 6600
Wire Wire Line
	1050 6600 1450 6600
Wire Wire Line
	1050 6850 1100 6850
Wire Wire Line
	2550 6200 2550 6250
Wire Wire Line
	2350 6150 2350 6200
Connection ~ 1050 6600
Connection ~ 2350 6200
Wire Wire Line
	2550 7500 2550 7450
Wire Wire Line
	2350 7450 2350 7500
Wire Wire Line
	1800 7450 1800 7500
Wire Wire Line
	1450 7450 1450 7500
Wire Wire Line
	1400 7050 1450 7050
Connection ~ 1450 7050
Wire Wire Line
	2350 6200 2550 6200
Wire Wire Line
	2850 4000 2850 4100
Wire Wire Line
	2500 4000 2500 4250
Wire Wire Line
	2500 4650 2500 4700
Wire Wire Line
	2850 4900 2850 4950
Wire Wire Line
	2850 5300 2850 5350
Wire Wire Line
	2500 5350 2500 5300
Wire Wire Line
	2550 4700 2500 4700
Connection ~ 2500 4700
Wire Wire Line
	2850 4400 2850 4450
Wire Wire Line
	2800 4450 2850 4450
Connection ~ 2850 4450
Wire Wire Line
	2000 6300 2000 6350
Wire Wire Line
	1900 5950 2000 5950
Wire Wire Line
	2000 5950 2000 6000
Connection ~ 2000 5950
Wire Wire Line
	1750 6350 2000 6350
Wire Wire Line
	1350 6350 1400 6350
Wire Wire Line
	1400 6350 1400 6300
Connection ~ 1400 6350
Wire Wire Line
	1400 5550 1400 5950
Wire Wire Line
	2350 4000 2350 4600
Wire Wire Line
	1050 5700 1050 6150
Connection ~ 1400 5950
$Comp
L power:GND #PWR05
U 1 1 5DDF01CD
P 2000 6400
F 0 "#PWR05" H 2000 6150 50  0001 C CNN
F 1 "GND" H 2000 6250 50  0001 C CNN
F 2 "" H 2000 6400 50  0001 C CNN
F 3 "" H 2000 6400 50  0001 C CNN
	1    2000 6400
	1    0    0    -1  
$EndComp
Connection ~ 2000 6350
$Comp
L headphone-amp-rescue:CP C6
U 1 1 5DDF0500
P 2750 6600
F 0 "C6" V 2900 6450 50  0000 L CNN
F 1 "1800µ" V 2800 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D8.0mm_P3.80mm" H 2788 6450 50  0001 C CNN
F 3 "" H 2750 6600 50  0001 C CNN
	1    2750 6600
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:CP C7
U 1 1 5DDF06F8
P 2750 7100
F 0 "C7" V 2900 6950 50  0000 L CNN
F 1 "1800µ" V 2800 6800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D8.0mm_P3.80mm" H 2788 6950 50  0001 C CNN
F 3 "" H 2750 7100 50  0001 C CNN
	1    2750 7100
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:R R16
U 1 1 5DDF08C4
P 2950 7300
F 0 "R16" V 3030 7300 50  0000 C CNN
F 1 "1k" V 2950 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2880 7300 50  0001 C CNN
F 3 "" H 2950 7300 50  0001 C CNN
	1    2950 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R18
U 1 1 5DDF0BF2
P 3150 7300
F 0 "R18" V 3230 7300 50  0000 C CNN
F 1 "1k" V 3150 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3080 7300 50  0001 C CNN
F 3 "" H 3150 7300 50  0001 C CNN
	1    3150 7300
	-1   0    0    1   
$EndComp
Wire Wire Line
	2900 7100 2950 7100
Wire Wire Line
	2950 7100 2950 7150
Wire Wire Line
	2900 6600 3150 6600
Wire Wire Line
	3150 6600 3150 7150
$Comp
L power:GND #PWR06
U 1 1 5DDF0E59
P 2950 7500
F 0 "#PWR06" H 2950 7250 50  0001 C CNN
F 1 "GND" H 2950 7350 50  0001 C CNN
F 2 "" H 2950 7500 50  0001 C CNN
F 3 "" H 2950 7500 50  0001 C CNN
	1    2950 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 7450 2950 7500
$Comp
L power:GND #PWR07
U 1 1 5DDF0F23
P 3150 7500
F 0 "#PWR07" H 3150 7250 50  0001 C CNN
F 1 "GND" H 3150 7350 50  0001 C CNN
F 2 "" H 3150 7500 50  0001 C CNN
F 3 "" H 3150 7500 50  0001 C CNN
	1    3150 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 7450 3150 7500
Connection ~ 2950 7100
Text Label 3500 7100 2    60   ~ 0
OUT_L-
Text Label 3500 6600 2    60   ~ 0
OUT_L+
Connection ~ 3150 6600
$Comp
L headphone-amp-rescue:Q_PNP_CBE Q2
U 1 1 5DE53506
P 1700 6850
F 0 "Q2" H 1900 6800 50  0000 L CNN
F 1 "BC557C" H 1900 6900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 1900 6950 50  0001 C CNN
F 3 "" H 1700 6850 50  0001 C CNN
	1    1700 6850
	1    0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:Q_PNP_CBE Q6
U 1 1 5DE5395A
P 2750 4700
F 0 "Q6" H 2950 4600 50  0000 L CNN
F 1 "BC557B" H 2950 4700 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 2950 4800 50  0001 C CNN
F 3 "" H 2750 4700 50  0001 C CNN
	1    2750 4700
	1    0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:Q_PNP_CBE Q5
U 1 1 5DE53B1F
P 2600 4450
F 0 "Q5" V 2650 4150 50  0000 L CNN
F 1 "BC557B" V 2550 4050 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 2800 4550 50  0001 C CNN
F 3 "" H 2600 4450 50  0001 C CNN
	1    2600 4450
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:Q_NPN_CBE Q1
U 1 1 5DE53CF4
P 1150 6350
F 0 "Q1" H 1450 6600 50  0000 L CNN
F 1 "BC547B" H 1250 6500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 1350 6450 50  0001 C CNN
F 3 "" H 1150 6350 50  0001 C CNN
	1    1150 6350
	-1   0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Q_NPN_ECB Q3
U 1 1 5DE54266
P 2250 5950
F 0 "Q3" H 2450 6000 50  0000 L CNN
F 1 "BD135" H 2450 5900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-126_Vertical" H 2450 6050 50  0001 C CNN
F 3 "" H 2250 5950 50  0001 C CNN
	1    2250 5950
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Q_PNP_CBE Q7
U 1 1 5DE96104
P 3450 4550
F 0 "Q7" V 3750 4400 50  0000 L CNN
F 1 "BC557B" V 3650 4400 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 3650 4650 50  0001 C CNN
F 3 "" H 3450 4550 50  0001 C CNN
	1    3450 4550
	0    1    -1   0   
$EndComp
$Comp
L headphone-amp-rescue:R R17
U 1 1 5DE9626A
P 3050 4450
F 0 "R17" V 2950 4450 50  0000 C CNN
F 1 "10k" V 3050 4450 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2980 4450 50  0001 C CNN
F 3 "" H 3050 4450 50  0001 C CNN
	1    3050 4450
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R19
U 1 1 5DE96452
P 3200 4950
F 0 "R19" V 3280 4950 50  0000 C CNN
F 1 "220k" V 3200 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3130 4950 50  0001 C CNN
F 3 "" H 3200 4950 50  0001 C CNN
	1    3200 4950
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:CP C8
U 1 1 5DE966C6
P 3700 5150
F 0 "C8" H 3725 5250 50  0000 L CNN
F 1 "47µ" H 3725 5050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 3738 5000 50  0001 C CNN
F 3 "" H 3700 5150 50  0001 C CNN
	1    3700 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 4950 3450 4950
Wire Wire Line
	2650 4950 2850 4950
Connection ~ 2850 4950
Wire Wire Line
	3700 4450 3700 4550
$Comp
L power:GND #PWR08
U 1 1 5DE969D9
P 3700 5350
F 0 "#PWR08" H 3700 5100 50  0001 C CNN
F 1 "GND" H 3700 5200 50  0001 C CNN
F 2 "" H 3700 5350 50  0001 C CNN
F 3 "" H 3700 5350 50  0001 C CNN
	1    3700 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5300 3700 5350
$Comp
L headphone-amp-rescue:Q_NPN_CBE Q9
U 1 1 5DE96B3B
P 4300 4950
F 0 "Q9" H 4500 5000 50  0000 L CNN
F 1 "BC547B" H 4500 4900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 4500 5050 50  0001 C CNN
F 3 "" H 4300 4950 50  0001 C CNN
	1    4300 4950
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R20
U 1 1 5DE96D83
P 3700 4700
F 0 "R20" V 3780 4700 50  0000 C CNN
F 1 "10k" V 3700 4700 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3630 4700 50  0001 C CNN
F 3 "" H 3700 4700 50  0001 C CNN
	1    3700 4700
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 4850 3700 4950
$Comp
L headphone-amp-rescue:R R21
U 1 1 5DE96F8A
P 3900 4950
F 0 "R21" V 3800 4950 50  0000 C CNN
F 1 "1k" V 3900 4950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3830 4950 50  0001 C CNN
F 3 "" H 3900 4950 50  0001 C CNN
	1    3900 4950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5DE971DC
P 4400 5350
F 0 "#PWR09" H 4400 5100 50  0001 C CNN
F 1 "GND" H 4400 5200 50  0001 C CNN
F 2 "" H 4400 5350 50  0001 C CNN
F 3 "" H 4400 5350 50  0001 C CNN
	1    4400 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5350 4400 5150
Wire Wire Line
	3750 4950 3700 4950
Connection ~ 3700 4950
Wire Wire Line
	4050 4950 4100 4950
Wire Wire Line
	3200 4450 3250 4450
Wire Wire Line
	3450 4950 3450 4750
Wire Wire Line
	3650 4450 3700 4450
$Comp
L headphone-amp-rescue:FINDER-30.22 K1
U 1 1 5DE97BD6
P 4600 4350
F 0 "K1" H 5450 4500 50  0000 L CNN
F 1 "FINDER-30.22" H 5450 4400 50  0000 L CNN
F 2 "Relays_ThroughHole:Relay_DPDT_Finder_40.52" H 6150 4320 50  0001 C CNN
F 3 "" H 4600 4350 50  0001 C CNN
	1    4600 4350
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:D D6
U 1 1 5DE980D5
P 4100 4050
F 0 "D6" H 4100 4150 50  0000 C CNN
F 1 "1N4148" H 4100 3950 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P10.16mm_Horizontal" H 4100 4050 50  0001 C CNN
F 3 "" H 4100 4050 50  0001 C CNN
	1    4100 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 4650 4400 4700
Wire Wire Line
	4400 4700 4100 4700
Wire Wire Line
	4100 4700 4100 4200
Connection ~ 4400 4700
Wire Wire Line
	4400 3850 4400 4050
Wire Wire Line
	4100 3850 4100 3900
Text Label 5750 3950 2    60   ~ 0
OUT_L-
Wire Wire Line
	5200 4650 5200 4700
Wire Wire Line
	5200 4700 5750 4700
Text Label 5750 4700 2    60   ~ 0
OUT_L+
Wire Wire Line
	5100 4050 5100 3950
Wire Wire Line
	5100 3950 5750 3950
Text Label 2650 4950 0    60   ~ 0
VREF
$Comp
L power:GND #PWR010
U 1 1 5DE99231
P 2500 5350
F 0 "#PWR010" H 2500 5100 50  0001 C CNN
F 1 "GND" H 2500 5200 50  0001 C CNN
F 2 "" H 2500 5350 50  0001 C CNN
F 3 "" H 2500 5350 50  0001 C CNN
	1    2500 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5DE992FA
P 2850 5350
F 0 "#PWR011" H 2850 5100 50  0001 C CNN
F 1 "GND" H 2850 5200 50  0001 C CNN
F 2 "" H 2850 5350 50  0001 C CNN
F 3 "" H 2850 5350 50  0001 C CNN
	1    2850 5350
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Q_NMOS_GDS Q12
U 1 1 5DE9A56C
P 5550 6850
F 0 "Q12" H 5750 6950 50  0000 L CNN
F 1 "IRLR024N" H 5750 6850 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2" H 5750 6950 50  0001 C CNN
F 3 "" H 5550 6850 50  0001 C CNN
	1    5550 6850
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R32
U 1 1 5DE9A572
P 5650 6400
F 0 "R32" V 5730 6400 50  0000 C CNN
F 1 "27" V 5650 6400 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 5580 6400 50  0001 C CNN
F 3 "" H 5650 6400 50  0001 C CNN
	1    5650 6400
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R30
U 1 1 5DE9A578
P 5450 6400
F 0 "R30" V 5530 6400 50  0000 C CNN
F 1 "27" V 5450 6400 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 5380 6400 50  0001 C CNN
F 3 "" H 5450 6400 50  0001 C CNN
	1    5450 6400
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R33
U 1 1 5DE9A57E
P 5650 7300
F 0 "R33" V 5730 7300 50  0000 C CNN
F 1 "27" V 5650 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 5580 7300 50  0001 C CNN
F 3 "" H 5650 7300 50  0001 C CNN
	1    5650 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R31
U 1 1 5DE9A584
P 5450 7300
F 0 "R31" V 5530 7300 50  0000 C CNN
F 1 "27" V 5450 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0309_L9.0mm_D3.2mm_P15.24mm_Horizontal" V 5380 7300 50  0001 C CNN
F 3 "" H 5450 7300 50  0001 C CNN
	1    5450 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R29
U 1 1 5DE9A58A
P 5100 7100
F 0 "R29" V 5000 7100 50  0000 C CNN
F 1 "10" V 5100 7100 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 7100 50  0001 C CNN
F 3 "" H 5100 7100 50  0001 C CNN
	1    5100 7100
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R27
U 1 1 5DE9A590
P 4900 7300
F 0 "R27" V 4980 7300 50  0000 C CNN
F 1 "2.2k" V 4900 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4830 7300 50  0001 C CNN
F 3 "" H 4900 7300 50  0001 C CNN
	1    4900 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R28
U 1 1 5DE9A596
P 5100 6600
F 0 "R28" V 5180 6600 50  0000 C CNN
F 1 "2.2k" V 5100 6600 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5030 6600 50  0001 C CNN
F 3 "" H 5100 6600 50  0001 C CNN
	1    5100 6600
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R24
U 1 1 5DE9A59C
P 4550 7300
F 0 "R24" V 4630 7300 50  0000 C CNN
F 1 "220k" V 4550 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4480 7300 50  0001 C CNN
F 3 "" H 4550 7300 50  0001 C CNN
	1    4550 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R22
U 1 1 5DE9A5A2
P 4350 6850
F 0 "R22" V 4430 6850 50  0000 C CNN
F 1 "150k" V 4350 6850 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4280 6850 50  0001 C CNN
F 3 "" H 4350 6850 50  0001 C CNN
	1    4350 6850
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:R R25
U 1 1 5DE9A5A8
P 4700 6600
F 0 "R25" V 4780 6600 50  0000 C CNN
F 1 "1k" V 4700 6600 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4630 6600 50  0001 C CNN
F 3 "" H 4700 6600 50  0001 C CNN
	1    4700 6600
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:CP C11
U 1 1 5DE9A5AE
P 4700 6350
F 0 "C11" V 4850 6150 50  0000 L CNN
F 1 "47µ" V 4750 6150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 4738 6200 50  0001 C CNN
F 3 "" H 4700 6350 50  0001 C CNN
	1    4700 6350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5DE9A5B4
P 5650 7500
F 0 "#PWR012" H 5650 7250 50  0001 C CNN
F 1 "GND" H 5650 7350 50  0001 C CNN
F 2 "" H 5650 7500 50  0001 C CNN
F 3 "" H 5650 7500 50  0001 C CNN
	1    5650 7500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5DE9A5BA
P 5450 7500
F 0 "#PWR013" H 5450 7250 50  0001 C CNN
F 1 "GND" H 5450 7350 50  0001 C CNN
F 2 "" H 5450 7500 50  0001 C CNN
F 3 "" H 5450 7500 50  0001 C CNN
	1    5450 7500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5DE9A5C0
P 4900 7500
F 0 "#PWR014" H 4900 7250 50  0001 C CNN
F 1 "GND" H 4900 7350 50  0001 C CNN
F 2 "" H 4900 7500 50  0001 C CNN
F 3 "" H 4900 7500 50  0001 C CNN
	1    4900 7500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5DE9A5C6
P 4550 7500
F 0 "#PWR015" H 4550 7250 50  0001 C CNN
F 1 "GND" H 4550 7350 50  0001 C CNN
F 2 "" H 4550 7500 50  0001 C CNN
F 3 "" H 4550 7500 50  0001 C CNN
	1    4550 7500
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:C C10
U 1 1 5DE9A5CC
P 4350 7050
F 0 "C10" V 4400 6850 50  0000 L CNN
F 1 "4.7µ" V 4500 6850 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L26.5mm_W7.0mm_P22.50mm_MKS4" H 4388 6900 50  0001 C CNN
F 3 "" H 4350 7050 50  0001 C CNN
	1    4350 7050
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:R R23
U 1 1 5DE9A5D2
P 4500 6150
F 0 "R23" V 4400 6150 50  0000 C CNN
F 1 "10k" V 4500 6150 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4430 6150 50  0001 C CNN
F 3 "" H 4500 6150 50  0001 C CNN
	1    4500 6150
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:R R26
U 1 1 5DE9A5D8
P 4850 5950
F 0 "R26" V 4750 5950 50  0000 C CNN
F 1 "2.2k" V 4850 5950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4780 5950 50  0001 C CNN
F 3 "" H 4850 5950 50  0001 C CNN
	1    4850 5950
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:CP C12
U 1 1 5DE9A5DE
P 5100 6150
F 0 "C12" H 5125 6250 50  0000 L CNN
F 1 "47µ" H 5125 6050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 5138 6000 50  0001 C CNN
F 3 "" H 5100 6150 50  0001 C CNN
	1    5100 6150
	1    0    0    -1  
$EndComp
Text Label 4500 5950 0    60   ~ 0
VREF
Text Label 3850 5700 0    60   ~ 0
Vunreg
Wire Wire Line
	5450 6550 5450 6600
Wire Wire Line
	5250 6600 5450 6600
Wire Wire Line
	5650 6550 5650 6600
Connection ~ 5650 6600
Wire Wire Line
	5650 7050 5650 7100
Wire Wire Line
	5450 7100 5650 7100
Wire Wire Line
	5450 7100 5450 7150
Connection ~ 5650 7100
Wire Wire Line
	4950 7100 4900 7100
Wire Wire Line
	4900 7050 4900 7100
Connection ~ 4900 7100
Connection ~ 5450 6600
Wire Wire Line
	5350 6850 5300 6850
Wire Wire Line
	5300 6850 5300 7100
Wire Wire Line
	5300 7100 5250 7100
Wire Wire Line
	4850 6600 4900 6600
Wire Wire Line
	4900 6600 4900 6650
Wire Wire Line
	4500 6850 4550 6850
Wire Wire Line
	4550 6850 4550 7050
Connection ~ 4550 6850
Connection ~ 4900 6600
Wire Wire Line
	4150 6550 4150 6600
Wire Wire Line
	4150 6600 4550 6600
Wire Wire Line
	4150 6850 4200 6850
Wire Wire Line
	5650 6200 5650 6250
Wire Wire Line
	5450 6150 5450 6200
Connection ~ 4150 6600
Connection ~ 5450 6200
Wire Wire Line
	5650 7500 5650 7450
Wire Wire Line
	5450 7450 5450 7500
Wire Wire Line
	4900 7450 4900 7500
Wire Wire Line
	4550 7450 4550 7500
Wire Wire Line
	4500 7050 4550 7050
Connection ~ 4550 7050
Wire Wire Line
	5450 6200 5650 6200
Wire Wire Line
	5100 6300 5100 6350
Wire Wire Line
	5000 5950 5100 5950
Wire Wire Line
	5100 5950 5100 6000
Connection ~ 5100 5950
Wire Wire Line
	4850 6350 5100 6350
Wire Wire Line
	4450 6350 4500 6350
Wire Wire Line
	4500 6350 4500 6300
Connection ~ 4500 6350
Wire Wire Line
	4500 5550 4500 5950
Wire Wire Line
	5450 5700 5450 5750
Wire Wire Line
	4150 5700 4150 6150
Connection ~ 4500 5950
Connection ~ 4150 5700
$Comp
L power:GND #PWR016
U 1 1 5DE9A628
P 5100 6400
F 0 "#PWR016" H 5100 6150 50  0001 C CNN
F 1 "GND" H 5100 6250 50  0001 C CNN
F 2 "" H 5100 6400 50  0001 C CNN
F 3 "" H 5100 6400 50  0001 C CNN
	1    5100 6400
	1    0    0    -1  
$EndComp
Connection ~ 5100 6350
$Comp
L headphone-amp-rescue:CP C13
U 1 1 5DE9A62F
P 5850 6600
F 0 "C13" V 6000 6400 50  0000 L CNN
F 1 "1800µ" V 5900 6300 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D8.0mm_P3.80mm" H 5888 6450 50  0001 C CNN
F 3 "" H 5850 6600 50  0001 C CNN
	1    5850 6600
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:CP C14
U 1 1 5DE9A635
P 5850 7100
F 0 "C14" V 6000 6900 50  0000 L CNN
F 1 "1800µ" V 5900 6800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D8.0mm_P3.80mm" H 5888 6950 50  0001 C CNN
F 3 "" H 5850 7100 50  0001 C CNN
	1    5850 7100
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:R R34
U 1 1 5DE9A63B
P 6050 7300
F 0 "R34" V 6130 7300 50  0000 C CNN
F 1 "1k" V 6050 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5980 7300 50  0001 C CNN
F 3 "" H 6050 7300 50  0001 C CNN
	1    6050 7300
	-1   0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:R R35
U 1 1 5DE9A641
P 6250 7300
F 0 "R35" V 6330 7300 50  0000 C CNN
F 1 "1k" V 6250 7300 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6180 7300 50  0001 C CNN
F 3 "" H 6250 7300 50  0001 C CNN
	1    6250 7300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 7100 6050 7100
Wire Wire Line
	6050 7100 6050 7150
Wire Wire Line
	6000 6600 6250 6600
Wire Wire Line
	6250 6600 6250 7150
$Comp
L power:GND #PWR017
U 1 1 5DE9A64D
P 6050 7500
F 0 "#PWR017" H 6050 7250 50  0001 C CNN
F 1 "GND" H 6050 7350 50  0001 C CNN
F 2 "" H 6050 7500 50  0001 C CNN
F 3 "" H 6050 7500 50  0001 C CNN
	1    6050 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 7450 6050 7500
$Comp
L power:GND #PWR018
U 1 1 5DE9A654
P 6250 7500
F 0 "#PWR018" H 6250 7250 50  0001 C CNN
F 1 "GND" H 6250 7350 50  0001 C CNN
F 2 "" H 6250 7500 50  0001 C CNN
F 3 "" H 6250 7500 50  0001 C CNN
	1    6250 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 7450 6250 7500
Connection ~ 6050 7100
Text Label 6600 7100 2    60   ~ 0
OUT_R-
Text Label 6600 6600 2    60   ~ 0
OUT_R+
Connection ~ 6250 6600
$Comp
L headphone-amp-rescue:Q_PNP_CBE Q10
U 1 1 5DE9A65F
P 4800 6850
F 0 "Q10" H 5000 6800 50  0000 L CNN
F 1 "BC557C" H 5000 6900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 5000 6950 50  0001 C CNN
F 3 "" H 4800 6850 50  0001 C CNN
	1    4800 6850
	1    0    0    1   
$EndComp
$Comp
L headphone-amp-rescue:Q_NPN_CBE Q8
U 1 1 5DE9A665
P 4250 6350
F 0 "Q8" H 4450 6400 50  0000 L CNN
F 1 "BC547B" H 4450 6300 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 4450 6450 50  0001 C CNN
F 3 "" H 4250 6350 50  0001 C CNN
	1    4250 6350
	-1   0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Q_NPN_ECB Q11
U 1 1 5DE9A66B
P 5350 5950
F 0 "Q11" H 5550 6000 50  0000 L CNN
F 1 "BD135" H 5550 5900 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-126_Vertical" H 5550 6050 50  0001 C CNN
F 3 "" H 5350 5950 50  0001 C CNN
	1    5350 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 5550 2650 5550
Wire Wire Line
	4500 5950 4700 5950
Wire Wire Line
	1400 5950 1600 5950
Wire Wire Line
	2650 4950 2650 5550
Connection ~ 2650 5550
Connection ~ 2350 5700
Connection ~ 2500 4000
Wire Wire Line
	1050 5700 2350 5700
Text Label 900  7050 0    60   ~ 0
IN_L
Wire Wire Line
	850  7050 1100 7050
Text Label 4000 7050 0    60   ~ 0
IN_R
Wire Wire Line
	3950 7050 4200 7050
Text Label 5750 4950 2    60   ~ 0
OUT_R+
Wire Wire Line
	5750 3750 4700 3750
Wire Wire Line
	4700 3750 4700 4050
Text Label 5750 3750 2    60   ~ 0
OUT_R-
Wire Wire Line
	4800 4650 4800 4950
Wire Wire Line
	4800 4950 5750 4950
$Comp
L headphone-amp-rescue:Earphone LS1
U 1 1 5DE9FE0B
P 3500 6800
F 0 "LS1" H 3300 6850 50  0000 L CNN
F 1 "Earphone" H 3200 6850 50  0001 L CNN
F 2 "Wire_Connections_Bridges:WireConnection_1.20mmDrill" V 3500 6900 50  0001 C CNN
F 3 "" V 3500 6900 50  0001 C CNN
	1    3500 6800
	1    0    0    1   
$EndComp
Wire Wire Line
	3500 6600 3500 6700
Wire Wire Line
	3500 7100 3500 7000
$Comp
L headphone-amp-rescue:Earphone LS2
U 1 1 5DEA0561
P 6600 6800
F 0 "LS2" H 6400 6850 50  0000 L CNN
F 1 "Earphone" H 6250 6950 50  0001 L CNN
F 2 "Wire_Connections_Bridges:WireConnection_1.20mmDrill" V 6600 6900 50  0001 C CNN
F 3 "" V 6600 6900 50  0001 C CNN
	1    6600 6800
	1    0    0    1   
$EndComp
Wire Wire Line
	6600 6600 6600 6700
Wire Wire Line
	6600 7100 6600 7000
$Comp
L headphone-amp-rescue:Conn_01x01 J3
U 1 1 5DEA0FAC
P 3950 6800
F 0 "J3" V 4050 6800 50  0000 C CNN
F 1 "IN_R" H 3950 6700 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_1-2mmDrill" H 3950 6800 50  0001 C CNN
F 3 "" H 3950 6800 50  0001 C CNN
	1    3950 6800
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:Conn_01x01 J2
U 1 1 5DEA14A1
P 850 6800
F 0 "J2" V 950 6800 50  0000 C CNN
F 1 "IN_L" H 850 6700 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_1-2mmDrill" H 850 6800 50  0001 C CNN
F 3 "" H 850 6800 50  0001 C CNN
	1    850  6800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 7000 3950 7050
Wire Wire Line
	850  7000 850  7050
$Comp
L headphone-amp-rescue:D_Bridge_+A-A D2
U 1 1 5DEA2026
P 1450 4650
F 0 "D2" V 1250 4800 50  0000 L CNN
F 1 "RB154" V 1450 4550 50  0000 L CNN
F 2 "Diodes_ThroughHole:Diode_Bridge_Round_D8.9mm" H 1450 4650 50  0001 C CNN
F 3 "" H 1450 4650 50  0001 C CNN
	1    1450 4650
	0    -1   -1   0   
$EndComp
$Comp
L headphone-amp-rescue:CP C3
U 1 1 5DEA2261
P 2000 4200
F 0 "C3" H 2025 4300 50  0000 L CNN
F 1 "2200µF" H 1850 4500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D13.0mm_P5.00mm" H 2038 4050 50  0001 C CNN
F 3 "" H 2000 4200 50  0001 C CNN
	1    2000 4200
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:CP C5
U 1 1 5DEA23E0
P 2200 4200
F 0 "C5" H 2225 4300 50  0000 L CNN
F 1 "2200µF" H 2200 4500 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D13.0mm_P5.00mm" H 2238 4050 50  0001 C CNN
F 3 "" H 2200 4200 50  0001 C CNN
	1    2200 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5DEA2D34
P 2000 4400
F 0 "#PWR019" H 2000 4150 50  0001 C CNN
F 1 "GND" H 2000 4250 50  0001 C CNN
F 2 "" H 2000 4400 50  0001 C CNN
F 3 "" H 2000 4400 50  0001 C CNN
	1    2000 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5DEA2E0B
P 2200 4400
F 0 "#PWR020" H 2200 4150 50  0001 C CNN
F 1 "GND" H 2200 4250 50  0001 C CNN
F 2 "" H 2200 4400 50  0001 C CNN
F 3 "" H 2200 4400 50  0001 C CNN
	1    2200 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4000 2200 4050
Connection ~ 2350 4000
Wire Wire Line
	2000 4000 2000 4050
Connection ~ 2200 4000
Connection ~ 2000 4000
$Comp
L power:GND #PWR021
U 1 1 5DEA3367
P 1450 5000
F 0 "#PWR021" H 1450 4750 50  0001 C CNN
F 1 "GND" H 1450 4850 50  0001 C CNN
F 2 "" H 1450 5000 50  0001 C CNN
F 3 "" H 1450 5000 50  0001 C CNN
	1    1450 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4350 2000 4400
Wire Wire Line
	2200 4400 2200 4350
$Comp
L headphone-amp-rescue:Conn_01x02 J1
U 1 1 5DEA3E0F
P 850 4650
F 0 "J1" H 850 4750 50  0000 C CNN
F 1 "PWR" H 850 4450 50  0000 C CNN
F 2 "Wire_Connections_Bridges:WireConnection_1.20mmDrill" H 850 4650 50  0001 C CNN
F 3 "" H 850 4650 50  0001 C CNN
	1    850  4650
	-1   0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Fuse F1
U 1 1 5DEA52A0
P 1800 4850
F 0 "F1" V 1880 4850 50  0000 C CNN
F 1 "Fuse" V 1725 4850 50  0000 C CNN
F 2 "Fuse_Holders_and_Fuses:Fuseholder5x20_horiz_open_inline_Type-I" V 1730 4850 50  0001 C CNN
F 3 "" H 1800 4850 50  0001 C CNN
	1    1800 4850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1450 4000 1450 4350
Wire Wire Line
	1050 4650 1100 4650
Wire Wire Line
	1750 4650 1800 4650
Wire Wire Line
	1800 4350 1800 4650
Wire Wire Line
	1800 5000 1800 5150
Wire Wire Line
	1800 5150 1100 5150
Wire Wire Line
	1100 5150 1100 4750
Wire Wire Line
	1100 4750 1050 4750
Wire Wire Line
	1450 4950 1450 5000
$Comp
L headphone-amp-rescue:R R9
U 1 1 5DEA8411
P 2100 4800
F 0 "R9" V 2180 4800 50  0000 C CNN
F 1 "2.2k" V 2100 4800 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2030 4800 50  0001 C CNN
F 3 "" H 2100 4800 50  0001 C CNN
	1    2100 4800
	-1   0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:LED D4
U 1 1 5DEA84D7
P 2100 5150
F 0 "D4" H 2100 5250 50  0000 C CNN
F 1 "LED" H 2100 5050 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 2100 5150 50  0001 C CNN
F 3 "" H 2100 5150 50  0001 C CNN
	1    2100 5150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5DEA860D
P 2100 5350
F 0 "#PWR022" H 2100 5100 50  0001 C CNN
F 1 "GND" H 2100 5200 50  0001 C CNN
F 2 "" H 2100 5350 50  0001 C CNN
F 3 "" H 2100 5350 50  0001 C CNN
	1    2100 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4950 2100 5000
Wire Wire Line
	2100 5300 2100 5350
Wire Wire Line
	2100 4650 2100 4600
Wire Wire Line
	2100 4600 2350 4600
Connection ~ 2350 4600
Wire Wire Line
	1450 4000 2000 4000
$Comp
L headphone-amp-rescue:D D3
U 1 1 5DEA9C12
P 1800 4200
F 0 "D3" H 1700 4250 50  0000 C CNN
F 1 "1N4148" H 1800 4100 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P10.16mm_Horizontal" H 1800 4200 50  0001 C CNN
F 3 "" H 1800 4200 50  0001 C CNN
	1    1800 4200
	0    1    1    0   
$EndComp
$Comp
L headphone-amp-rescue:D D1
U 1 1 5DEA9D4A
P 1100 4200
F 0 "D1" H 1100 4300 50  0000 C CNN
F 1 "1N4148" H 1100 4100 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P10.16mm_Horizontal" H 1100 4200 50  0001 C CNN
F 3 "" H 1100 4200 50  0001 C CNN
	1    1100 4200
	0    1    1    0   
$EndComp
Connection ~ 1800 4650
Wire Wire Line
	1100 4350 1100 4650
Connection ~ 1100 4650
Wire Wire Line
	1100 4050 1100 3850
Wire Wire Line
	1100 3850 1800 3850
Connection ~ 4100 3850
Wire Wire Line
	1800 4050 1800 3850
Connection ~ 1800 3850
$Comp
L headphone-amp-rescue:CP C9
U 1 1 5DEAA4EE
P 3800 4050
F 0 "C9" H 3550 4100 50  0000 L CNN
F 1 "47µ" H 3550 4000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D6.3mm_P2.50mm" H 3838 3900 50  0001 C CNN
F 3 "" H 3800 4050 50  0001 C CNN
	1    3800 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5DEAA734
P 3800 4250
F 0 "#PWR023" H 3800 4000 50  0001 C CNN
F 1 "GND" H 3800 4100 50  0001 C CNN
F 2 "" H 3800 4250 50  0001 C CNN
F 3 "" H 3800 4250 50  0001 C CNN
	1    3800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4200 3800 4250
Wire Wire Line
	3800 3900 3800 3850
Connection ~ 3800 3850
Wire Notes Line
	750  3650 750  7650
Wire Notes Line
	750  7650 6750 7650
Wire Notes Line
	6750 7650 6750 3650
Wire Notes Line
	6750 3650 750  3650
Wire Notes Line
	6000 3650 6000 5500
Text Notes 800  3750 0    60   ~ 0
Click Protection
Wire Notes Line
	750  6550 2250 6550
Wire Notes Line
	2250 6550 2250 6150
Wire Notes Line
	2250 6150 3750 6150
Wire Notes Line
	3750 6550 5350 6550
Wire Notes Line
	5350 6550 5350 6150
Wire Notes Line
	5350 6150 6750 6150
Wire Notes Line
	3750 6150 3750 7650
Wire Notes Line
	6750 5500 2950 5500
Wire Notes Line
	2400 5500 750  5500
Text Notes 2700 5650 0    60   ~ 0
Supply Ripple Rejection
Wire Notes Line
	1900 3950 2950 3950
Wire Notes Line
	2950 3950 2950 5500
Wire Notes Line
	2400 3950 2400 5500
Wire Notes Line
	1900 3950 1900 4400
Wire Notes Line
	1900 4400 750  4400
Text Notes 800  5450 0    60   ~ 0
Powersupply
Text Notes 2650 6250 0    60   ~ 0
Left Channel Amp
Text Notes 5800 6250 0    60   ~ 0
Right Channel Amp
$Comp
L headphone-amp-rescue:Conn_01x01 J4
U 1 1 5E089DE6
P 850 7250
F 0 "J4" V 950 7250 50  0000 C CNN
F 1 "Conn_01x01" H 850 7150 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_1-2mmDrill" H 850 7250 50  0001 C CNN
F 3 "" H 850 7250 50  0001 C CNN
	1    850  7250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5E089EE9
P 850 7500
F 0 "#PWR024" H 850 7250 50  0001 C CNN
F 1 "GND" H 850 7350 50  0001 C CNN
F 2 "" H 850 7500 50  0001 C CNN
F 3 "" H 850 7500 50  0001 C CNN
	1    850  7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  7450 850  7500
$Comp
L headphone-amp-rescue:Conn_01x01 J5
U 1 1 5E08A3EA
P 3950 7250
F 0 "J5" V 4050 7250 50  0000 C CNN
F 1 "Conn_01x01" H 3950 7150 50  0001 C CNN
F 2 "Wire_Pads:SolderWirePad_single_1-2mmDrill" H 3950 7250 50  0001 C CNN
F 3 "" H 3950 7250 50  0001 C CNN
	1    3950 7250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5E08A586
P 3950 7500
F 0 "#PWR025" H 3950 7250 50  0001 C CNN
F 1 "GND" H 3950 7350 50  0001 C CNN
F 2 "" H 3950 7500 50  0001 C CNN
F 3 "" H 3950 7500 50  0001 C CNN
	1    3950 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 7450 3950 7500
$Comp
L headphone-amp-rescue:Mounting_Hole MK4
U 1 1 5E08CA10
P 6400 5350
F 0 "MK4" H 6400 5550 50  0000 C CNN
F 1 "M3" H 6400 5475 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 6400 5350 50  0001 C CNN
F 3 "" H 6400 5350 50  0001 C CNN
	1    6400 5350
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Mounting_Hole MK3
U 1 1 5E08D36B
P 6400 4950
F 0 "MK3" H 6400 5150 50  0000 C CNN
F 1 "M3" H 6400 5075 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 6400 4950 50  0001 C CNN
F 3 "" H 6400 4950 50  0001 C CNN
	1    6400 4950
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Mounting_Hole MK2
U 1 1 5E08D47F
P 6400 4550
F 0 "MK2" H 6400 4750 50  0000 C CNN
F 1 "M3" H 6400 4675 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 6400 4550 50  0001 C CNN
F 3 "" H 6400 4550 50  0001 C CNN
	1    6400 4550
	1    0    0    -1  
$EndComp
$Comp
L headphone-amp-rescue:Mounting_Hole MK1
U 1 1 5E08D59C
P 6400 4150
F 0 "MK1" H 6400 4350 50  0000 C CNN
F 1 "M3" H 6400 4275 50  0000 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965" H 6400 4150 50  0001 C CNN
F 3 "" H 6400 4150 50  0001 C CNN
	1    6400 4150
	1    0    0    -1  
$EndComp
Text Notes 6000 3750 0    60   ~ 0
Mounting Holes
Wire Wire Line
	2550 6600 2600 6600
Wire Wire Line
	2550 6600 2550 6650
Wire Wire Line
	2550 7100 2550 7150
Wire Wire Line
	2550 7100 2600 7100
Wire Wire Line
	1800 7100 1800 7150
Wire Wire Line
	2350 6600 2550 6600
Wire Wire Line
	1450 6850 1500 6850
Wire Wire Line
	1800 6600 1850 6600
Wire Wire Line
	1050 6600 1050 6850
Wire Wire Line
	2350 6200 2350 6250
Wire Wire Line
	1450 7050 1450 7150
Wire Wire Line
	2500 4700 2500 5000
Wire Wire Line
	2850 4450 2850 4500
Wire Wire Line
	2850 4450 2900 4450
Wire Wire Line
	2000 5950 2050 5950
Wire Wire Line
	1400 6350 1450 6350
Wire Wire Line
	1400 5950 1400 6000
Wire Wire Line
	2000 6350 2000 6400
Wire Wire Line
	2950 7100 3500 7100
Wire Wire Line
	3150 6600 3500 6600
Wire Wire Line
	2850 4950 2850 5000
Wire Wire Line
	2850 4950 3050 4950
Wire Wire Line
	3700 4950 3700 5000
Wire Wire Line
	4400 4700 4400 4750
Wire Wire Line
	5650 6600 5700 6600
Wire Wire Line
	5650 6600 5650 6650
Wire Wire Line
	5650 7100 5650 7150
Wire Wire Line
	5650 7100 5700 7100
Wire Wire Line
	4900 7100 4900 7150
Wire Wire Line
	5450 6600 5650 6600
Wire Wire Line
	4550 6850 4600 6850
Wire Wire Line
	4900 6600 4950 6600
Wire Wire Line
	4150 6600 4150 6850
Wire Wire Line
	5450 6200 5450 6250
Wire Wire Line
	4550 7050 4550 7150
Wire Wire Line
	5100 5950 5150 5950
Wire Wire Line
	4500 6350 4550 6350
Wire Wire Line
	4500 5950 4500 6000
Wire Wire Line
	4150 5700 5450 5700
Wire Wire Line
	5100 6350 5100 6400
Wire Wire Line
	6050 7100 6600 7100
Wire Wire Line
	6250 6600 6600 6600
Wire Wire Line
	2650 5550 4500 5550
Wire Wire Line
	2350 5700 2350 5750
Wire Wire Line
	2350 5700 4150 5700
Wire Wire Line
	2500 4000 2850 4000
Wire Wire Line
	2350 4000 2500 4000
Wire Wire Line
	2200 4000 2350 4000
Wire Wire Line
	2000 4000 2200 4000
Wire Wire Line
	2350 4600 2350 5700
Wire Wire Line
	1800 4650 1800 4700
Wire Wire Line
	1100 4650 1150 4650
Wire Wire Line
	4100 3850 4400 3850
Wire Wire Line
	1800 3850 3800 3850
Wire Wire Line
	3800 3850 4100 3850
$EndSCHEMATC
